#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

struct image {
    size_t width, height;
    struct pixel *data;
};

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image malloc_image(uint32_t width, uint32_t height);

void free_image(const struct image* img);

void set_pixel(struct image* img, size_t x_pos, size_t y_pos, struct pixel source_pixel );

struct pixel get_pixel(struct image img, size_t x_pos, size_t y_pos);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
