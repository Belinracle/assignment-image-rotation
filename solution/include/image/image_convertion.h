#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_CONVERTER_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_CONVERTER_H

#include "../convertors/rotate.h"

typedef struct image transform(struct image img);

enum convertors{
    ROTATION = 0
};

extern transform* transformers[];

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_CONVERTER_H
