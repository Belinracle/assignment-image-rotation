#ifndef ASSIGNMENT_IMAGE_ROTATION_FORMATS_H
#define ASSIGNMENT_IMAGE_ROTATION_FORMATS_H

#include "bmp/bmp_processor.h"

typedef enum status reader(FILE*, struct image*);
typedef enum status writer(FILE*, struct image*);

enum format {
    BMP = 0
};

extern reader* readers[];

extern writer* writers[];

#endif //ASSIGNMENT_IMAGE_ROTATION_FORMATS_H
