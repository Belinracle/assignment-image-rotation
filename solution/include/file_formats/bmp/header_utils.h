#ifndef ASSIGNMENT_IMAGE_ROTATION_HEADER_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_HEADER_UTILS_H

#include "bmp_processor.h"
#include <inttypes.h>
#include <stdio.h>

uint8_t calc_padding(uint32_t width);

struct bmp_header create_bmp_header(uint32_t width, uint32_t height);

enum status read_bmp_header(FILE *f, struct bmp_header *header);

#endif //ASSIGNMENT_IMAGE_ROTATION_HEADER_UTILS_H
