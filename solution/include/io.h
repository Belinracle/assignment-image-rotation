#ifndef ASSIGNMENT_IMAGE_ROTATION_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_IO_H

#include "file_formats/formats.h"
#include "image/image.h"
#include "status_utils/status.h"
#include "stdio.h"
#include <stdbool.h>

bool open_file(FILE** file, const char* name, const char* mode);

bool close_file(FILE** file);

bool load_image_from_file(const char* filename, struct image* image, reader rdr);

bool write_image_to_file(const char* filename, struct image* image, writer wrtr);

#endif //ASSIGNMENT_IMAGE_ROTATION_IO_H
