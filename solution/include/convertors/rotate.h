#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "../image/image.h"

struct image rotate(struct image img);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
