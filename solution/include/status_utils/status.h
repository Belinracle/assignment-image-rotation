#ifndef ASSIGNMENT_IMAGE_ROTATION_ENUMS_H
#define ASSIGNMENT_IMAGE_ROTATION_ENUMS_H

#include "stdbool.h"
#include "stdio.h"

enum status{
    READ_SUCCESS=0,
    WRITE_SUCCESS,
    READ_ERROR,
    WRITE_ERROR,
    OPEN_ERROR,
    CLOSE_ERROR,
    INVALID_HEADER_SIGNATURE,
    ERROR_READING_HEADER,
    HEADER_OK
};

bool process_status(enum status status);

void print_status(enum status status);

#endif //ASSIGNMENT_IMAGE_ROTATION_ENUMS_H
