#include "../include/status_utils/status.h"

static const char *const get_status_description[] = {
        [READ_SUCCESS]="Successfully read",
        [WRITE_SUCCESS]="Successfully wrote",
        [READ_ERROR]="Cant read",
        [WRITE_ERROR]="Cant write",
        [OPEN_ERROR]="Cant open",
        [CLOSE_ERROR]="Cant close",
        [INVALID_HEADER_SIGNATURE]="Bad header",
        [HEADER_OK]="Header OK",
        [ERROR_READING_HEADER]="Error while reading header"
};

bool process_status(enum status status) {
    print_status(status);
    return !(status == WRITE_ERROR || status == READ_ERROR || status == INVALID_HEADER_SIGNATURE);
}

void print_status(enum status status) {
    printf("%s", get_status_description[status]);
}
