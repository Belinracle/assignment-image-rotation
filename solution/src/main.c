#include "../include/image/image_convertion.h"
#include "../include/io.h"
#include <stdio.h>

void error(char *msg) {
    fprintf(stderr, "%s", msg);
}

int main(int argc, char **argv) {
    if (argc < 3) {
        error("not enough args\n");
        return 0;
    }
    char *const in_filename = argv[1];
    char *const out_filename = argv[2];

    struct image img = {0};
    enum format format = BMP;
    enum convertors action = ROTATION;

    if (!load_image_from_file(in_filename, &img, readers[format])) {
        return 1;
    }

    struct image result = transformers[action](img);
    free_image(&img);

    bool write_success = write_image_to_file(out_filename, &result, writers[format]);
    free_image(&result);

    return !write_success;
}
