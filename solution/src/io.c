#include "../include/io.h"

bool open_file(FILE** file, const char* name, const char* mode){
    *file = fopen(name,mode);
    return (*file)!=NULL;
}

bool close_file(FILE** file){
    return fclose(*file)==0;
}

bool load_image_from_file(const char* filename, struct image* image, reader rdr){
    FILE* file;
    if(open_file(&file,filename,"rb")) {
        bool read_result = process_status(rdr(file, image));
        bool close_result = close_file(&file);
        return read_result && close_result;
    }
    return false;

}

bool write_image_to_file(const char* filename, struct image* image, writer wrtr){
    FILE* file;
    if(open_file(&file,filename,"wb")){
        bool write_result = process_status(wrtr(file,image));
        bool close_result = close_file(&file);
        return write_result && close_result;
    }
    return false;
}
