#include "../include/image/image.h"
#include "malloc.h"

struct image malloc_image(uint32_t width, uint32_t height){
    struct pixel* pixel = malloc(width * height * sizeof(struct pixel));
    return (struct image) {.width=width, .height=height, .data=pixel};
}

void free_image(const struct image* img){
    free(img->data);
}

void set_pixel(struct image* img, size_t x_pos, size_t y_pos, struct pixel source_pixel ){
    img->data[img->width*y_pos+x_pos] = source_pixel;
}

struct pixel get_pixel(struct image img, size_t x_pos, size_t y_pos){
    return img.data[img.width*y_pos+x_pos];
}
