#include "../../include/convertors/rotate.h"
struct image rotate(struct image img){
    uint32_t out_width = img.height;

    struct image result = malloc_image(img.height,img.width);

    for (uint32_t row = 0; row < img.height; row++){
        for (uint32_t column = 0; column < img.width; column++){
            set_pixel(&result,out_width-row-1,column, get_pixel(img,column,row));
        }
    }
    return result;
}
