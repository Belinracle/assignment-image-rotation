#include "../include/file_formats/bmp/header_utils.h"

const uint32_t BMP_SIGNATURE = 0x4D42;
const size_t BIT_PER_COLOR = 24;
const uint16_t PLANES = 1;
const uint32_t PIXEL_PER_M = 2834;


uint8_t calc_padding(uint32_t width) {
    return (4 - (width * sizeof(struct pixel) % 4)) % 4;
}

enum status is_header_valid(const struct bmp_header header){
    return (header.bfType!=BMP_SIGNATURE    ||
            header.biSize!=40               ||
            header.biBitCount!=BIT_PER_COLOR) ? INVALID_HEADER_SIGNATURE : HEADER_OK;
}

enum status read_bmp_header(FILE* f, struct bmp_header* header){
    if (fread(header, sizeof(struct bmp_header), 1, f) < 1) return ERROR_READING_HEADER;
    return is_header_valid(*header);
}

struct bmp_header create_bmp_header(uint32_t width, uint32_t height){
    const size_t header_size = sizeof(struct bmp_header);
    const size_t image_size = height * (width + calc_padding(width)) * sizeof(struct pixel);


    struct bmp_header header = {
            .bfType = BMP_SIGNATURE,
            .biHeight = height,
            .biWidth = width,
            .bOffBits = header_size,
            .biSize = 40,
            .biPlanes = PLANES,
            .bfileSize = header_size + image_size,
            .biCompression = 0,
            .bfReserved = 0,
            .biBitCount = BIT_PER_COLOR,
            .biXPelsPerMeter = PIXEL_PER_M,
            .biYPelsPerMeter = PIXEL_PER_M,
            .biClrUsed = 0,
            .biClrImportant = 0,
            .biSizeImage = image_size
    };

    return header;
}
