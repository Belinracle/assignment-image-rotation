#include "../../include/file_formats/bmp/bmp_processor.h"

enum status write_header(struct bmp_header *header, FILE *out) {
    return fwrite(header, sizeof(struct bmp_header), 1, out) < 1 ? WRITE_ERROR : WRITE_SUCCESS;
}

enum status write_body(FILE *out, struct image *img) {
    const size_t padding = calc_padding(img->width);
    const uint32_t zero = 0; // to fill padding
    for (uint32_t row = 0; row < img->height; row++) {
        if (fwrite(&img->data[row * img->width], sizeof(struct pixel), img->width, out)
            < img->width)
            return WRITE_ERROR;
        if (fwrite(&zero, padding, 1, out) < 1) return WRITE_ERROR;
    }
    return WRITE_SUCCESS;
}


enum status from_bmp(FILE *input, struct image *img) {
    struct bmp_header header = {0};
    enum status read_header_status = read_bmp_header(input, &header);
    if (read_header_status != HEADER_OK) {
        return read_header_status;
    }
    *img = malloc_image(header.biWidth, header.biHeight);

    const size_t padding = calc_padding(header.biWidth);

    for (uint64_t i = 0; i < img->height; i++) {
        void *start_index = img->data + img->width * i;
        if (fread(start_index, sizeof(struct pixel), img->width, input) < img->width) return ERROR_READING_HEADER;
        if (fseek(input, padding, SEEK_CUR) != 0) return ERROR_READING_HEADER;
    }
    return READ_SUCCESS;
}

enum status to_bmp(FILE *out, struct image *img) {
    struct bmp_header header = create_bmp_header(img->width, img->height);
    return (process_status(write_header(&header, out)) &&
            process_status(write_body(out, img))) ? WRITE_SUCCESS : WRITE_ERROR;

}
