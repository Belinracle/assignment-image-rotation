#include "file_formats/formats.h"

reader* readers[] = {
        [BMP] = from_bmp,
};

writer* writers[] = {
        [BMP] = to_bmp
};
